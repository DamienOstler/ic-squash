module{
    public type ProjectModel = {
        id:Nat;
        name:Text;
        description:Text;
        created:Int;
        updated:Int;
    };
}