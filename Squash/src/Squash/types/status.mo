module{
    public type Status = {
        #unassigned;
        #assigned;
        #in_progress;
        #waiting_reporter;
        #waiting_assignee;
    };
};