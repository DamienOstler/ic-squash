import Ticket "../classes/ticket";
import Time "mo:base/Time";
module{
    public class Comment(commentIdentifier:Nat,
                commentTicket:Ticket.Ticket,
                commentContent:Text,
                commentCreated:Time.Time,
                commentUpdated:Time.Time){
        
        //the reporter of the ticket
        var identifier = commentIdentifier;
        var ticket = commentTicket;
        var content = commentContent;
        var created = commentCreated;
        var updated = commentUpdated;
    };
};