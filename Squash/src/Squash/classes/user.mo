import Role "../types/role";
module{
    public class User(userId:Principal,userName:Text,userRole:Role.Role){

        var id = userId;
        var name = userName;
        var role = userRole;

        public func getId():Principal{ id };
        public func getName():Text{ name };
        public func getRole():Role.Role{ role };

        public func setName(newName:Text){ name := newName };
        public func setRole(newRole:Role.Role){ role := newRole };
    };
};