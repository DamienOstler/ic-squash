import Project "../classes/project";
import Time "mo:base/Time";
module{
    public class Ticket(ticketIdentifier:Nat,
                ticketProject:Project.Project,
                ticketTitle:Text,
                ticketSummary:Text,
                ticketPriority:Text,
                ticketStatus:Text,
                ticketCategory:Text,
                ticketCreated:Time.Time,
                ticketUpdated:Time.Time){
        var identifier = ticketIdentifier;
        var project = ticketProject;
        var title = ticketTitle;
        var summary = ticketSummary;
        var priority = ticketPriority;
        var status = ticketStatus;
        var category = ticketCategory;
        var created = ticketCreated;
        var updated = ticketUpdated;


        public func updateTitle(){

        };

        public func updateSummary(){

        };

        public func updatePriority(){

        };

        public func updateStatus(){

        };

        public func updateCategory(){

        };

        //Comment Functions
        public func addComment(){

        };
    };
};