import User "../classes/user";
import Role "../types/role";
import Principal "mo:base/Principal";
import HashMap "mo:base/HashMap";
import Hash "mo:base/Hash";
import Time "mo:base/Time";
module{
    public class Project(projectIdentifier:Nat,projectName:Text,projectDescription:Text,projectCreated:Int,projectUpdated:Int){
        
        var identifier = projectIdentifier;
        var name = projectName;
        var description = projectDescription;
        var created = projectCreated;
        var updated = projectUpdated;

        func principalEqual(l:Principal, r:Principal): Bool { l==r };
        //func updated(){ updated := Time.Time.Now; };

        var users : HashMap.HashMap<Principal, User.User> = HashMap.HashMap<Principal, User.User>(1,principalEqual,Principal.hash);
        
        public func getIdentifier():Nat{ identifier };
        public func getName():Text{ name };
        public func getDescription():Text{ description };
        public func getCreated():Int{ created };
        public func getUpdated():Int{ updated };
        public func getUsers(): HashMap.HashMap<Principal, User.User>{users};

        public func updateName(newName:Text){ name:=newName; };
        public func updateDescription(newDesc:Text){ description:=newDesc;  };
        
        public func addUser(newUser:User.User){ 
            let id = newUser.getId();
            ignore(users.replace(id,newUser));
        };
        public func remUser(){ /*todo not sure how to do this yet.*/ };


        public func addTicket(){ };
        public func remTicket(){ };
        public func getTicket(){ };
    };
};