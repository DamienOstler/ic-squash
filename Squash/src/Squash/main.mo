import Project "./classes/project";
import User "./classes/user";
import Role "./types/role";
import ProjectModel "./types/projectmodel";
import HashMap "mo:base/HashMap";
import Hash "mo:base/Hash";
import Array "mo:base/Array";
import Time "mo:base/Time";

actor {
    var projectCount : Nat = 0;
    

    func projectIdEq(l:Nat, r:Nat):Bool{ return l==r; };
    var projects : HashMap.HashMap<Nat, Project.Project> = HashMap.HashMap<Nat, Project.Project>(1,projectIdEq,Hash.hash);

    public shared(msg) func createProject(name:Text,description:Text):async ProjectModel.ProjectModel{
        projectCount := projectCount+1;
        let project = Project.Project(projectCount,name,description,Time.now(),Time.now());
        let rootUser:User.User = User.User(msg.caller, "Owner", #admin );
        project.addUser(rootUser);
        ignore(projects.replace(projectCount,project));
        let newModel:ProjectModel.ProjectModel = {
            id=projectCount;
            name = project.getName();
            description = project.getDescription();
            created = project.getCreated();
            updated = project.getUpdated();
        };
        return newModel;
    };

    public shared(msg) func getProjects(): async [ProjectModel.ProjectModel] {
        var usersProjects : [ProjectModel.ProjectModel] = [];
        for((id,project) in projects.entries()){
            let newModel:ProjectModel.ProjectModel = {
                id=projectCount;
                name = project.getName();
                description = project.getDescription();
                created = project.getCreated();
                updated = project.getUpdated();
            };
            for((userId,user) in project.getUsers().entries()){
                if(userId==msg.caller)
                {
                    usersProjects := Array.append<ProjectModel.ProjectModel>(usersProjects,[newModel]);
                };
            };
        };
        return usersProjects;
    };
    
};
