import { Actor, HttpAgent } from '@dfinity/agent';
import { idlFactory as Squash_idl, canisterId as Squash_id } from 'dfx-generated/Squash';

const agent = new HttpAgent();
const Squash = Actor.createActor(Squash_idl, { agent, canisterId: Squash_id });

document.getElementById("clickMeBtn").addEventListener("click", async () => {
  const name = document.getElementById("name").value.toString();
  const greeting = await Squash.greet(name);

  document.getElementById("greeting").innerText = greeting;
});
