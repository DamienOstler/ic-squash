# Squash - Bug tracker on the Internet Computer.

This project will be a fully fledged and fully open source issue tracking platform that will run on the Internet Computer. I am using this as a learning opportunity to get into block chain development, and really just learn more about cutting edge technologies. If you have any suggestions for futures please let me know! I want to get something functional for the first version before the scope starts getting too big.

Discord : Damien#3456
Email : damienostler1@outlook.com


## Features Roadmap for  0.1.0

 - [x] Administrators will be able to create their own service desks on the application.
 - [ ] Administrators will be able to delete their own service desks or transfer ownership.
 - [ ] Administrators will be able to assign users to their service desk.
 - [ ] Administrators will be able to invite reporters to their service desk.
 - [ ] Administrators will be able to assign roles to their users. ( Support, Developer, Manager, Administrator )
 - [ ] Managers will be able to assign users to tickets.
 - [ ] Managers will be able to set the priority level of the tickets.
 - [ ] Users will be able to set the status of the tickets.
 - [ ] Users will be able to set the type of the tickets.
 - [ ] Users will be able to comment on the tickets to respond to reporters.
 - [ ] Users will be able to comment on the tickets to create internal notes.
 - [ ] Users will be able to request a manager to look at a ticket.
 - [ ] Reporters will be able to create tickets on boards they have been invited or added to.
 - [ ] Reporters will be able to comment on tickets that they have created.
 - [ ] Reporters will be able to close an ticket early.



## Interface Wireframes
